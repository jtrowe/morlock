package Morlock;


# ABSTRACT: A base class for a customized App::Cmd application

# COPYRIGHT


use Moose;

extends qw( MooseX::App::Cmd );

with qw( Morlock::Configurable );
with qw( Morlock::ConfigurationLoader );
with qw( Morlock::Loggable );
with qw( Morlock::LoggingInitializer );


use Data::Dump qw( dump );
use File::Spec;


=method default_name

Returns the default application name.

It is derived from the last part of the full class name.

=cut

sub default_name {
    my $self = shift;

    my @parts = ref $self;

    return $parts[-1];
} # default_name


=method get_env_option

Returns the value of a program option as supplied by the environment.

B<WARNING> This is likely not portable.

Parameters:

=over

=item * C<option name> String

The option name, the same as it would be on the command line.
example: the C<config-file> option would be specified as
"config-file" to this method or as --config-file on the command line.

=item * C<env name> Hash Reference

The environment hash to read from.
If none is supplied then C<%ENV> will be used.

=back


Returns:

=over

=item * C<option value> String

May be undefined.

=back

=cut

sub get_env_option {
    my $self   = shift;
    my $option = shift;
    my $env    = shift // \%ENV;

    # FIXME OS portability ?

    my $env_key = uc(join '-', $self->name, $option);
    $env_key =~ s/-/_/g;

    $self->log->debug("env_key=$env_key");

    my $value = $env->{$env_key};

    $self->log->debug("env_val=" . ( $value // '' ));

    return $value;
} # get_env_option


=method global_opt_spec

Return the global option specification for this application.

=cut

sub global_opt_spec {
    return (
        [
            'config-file|c=s',
            'Specifies the name of the configuration file to load.'
            . '  The config-path will not be used.',
        ],
        [
            'config-name=s',
            'Specifies the name of the configuration file to search '
            . 'for on the config-path.',
        ],
        [
            'config-path=s',
            'The path to search for a configuration file.',
        ],
        [
            'log-config-file=s',
            'Specifies the name of the logging configuration file to use.',
        ],
        [
            'log-level=s',
            'Specifies the name of the root log level.  '
                    . 'Should be one of the following: '
                    . join(' ', qw( OFF FATAL ERROR WARN INFO DEBUG TRACE ALL )),
        ],
        [
            'quiet|q+',
            'Decreases the logging detail.',
        ],
        [
            'verbose|v+',
            'Increases the logging detail.',
        ],
    );
} # global_opt_spec


override 'prepare_command' => sub {
    my $self = shift;

    $self->initialize_bootstrap_logging;

    my @results = super;

    $self->log->debug("global_options:\n" . dump($self->global_options))
            if $self->log->is_debug;

    $self->process_global_options;

    $self->config;

    $self->initialize_logging;

    return @results;
}; # prepare_command


=method process_env_options

Attempts to load options for this application from the environment.

=cut

sub process_env_options {
    my $self = shift;
    my $env   = shift // \%ENV;

    if ( my $file = $self->get_env_option('config_file', $env) ) {
        $self->config_file($file);
        $self->config_name('');
        $self->config_path([]);
    }
    else {
        if ( my $name = $self->get_env_option('config_name', $env) ) {
            $self->config_name($name);
        }

        if ( my $path = $self->get_env_option('config_path', $env) ) {
            my $separator = ':';
            my @dirs = split $separator, $path;
            $self->config_path(\@dirs);
        }

    }

    if ( my $log_file = $self->get_env_option('log_config_file', $env) ) {
        unless ( File::Spec->file_name_is_absolute($log_file) ) {
            $log_file = File::Spec->rel2abs($log_file);
        }
        $self->logging_config_file($log_file);
    }

    if ( my $level = $self->log_level ) {
        $self->set_log_level($level);
    }

    if ( my $level = $self->get_env_option('log_level', $env) ) {
        $self->log_level($level);
    }

    if ( my $quiet = $self->get_env_option('quiet', $env) ) {
        $self->quietness($quiet);
    }

    if ( my $verbose = $self->get_env_option('verbose', $env) ) {
        $self->verbosity($verbose);
    }

} # process_env_options


=method process_global_options

Attempts to load options for this application from the command line
options.

=cut

sub process_global_options {
    my $self = shift;

    if ( my $file = $self->global_options->config_file ) {
        $self->config_file($file);
        $self->config_name('');
        $self->config_path([]);
    }
    else {
        # TODO: document this behavior

        if ( my $name = $self->global_options->config_name ) {
            $self->config_name($name);
        }

        if ( my $path = $self->global_options->config_path ) {
            my $separator = ':';
            my @dirs = split $separator, $path;
            $self->config_path(\@dirs);
        }
    }

    if ( my $level = $self->global_options->log_level ) {
        $self->log_level($level);
    }

    if ( my $quiet = $self->global_options->quiet ) {
        $self->quietness($quiet);
    }

    if ( my $verbose = $self->global_options->verbose ) {
        $self->verbosity($verbose);
    }

    if ( my $log_file = $self->global_options->log_config_file ) {
        unless ( File::Spec->file_name_is_absolute($log_file) ) {
            $log_file = File::Spec->rel2abs($log_file);
        }
        $self->logging_config_file($log_file);
    }

    if (
        ( ! $self->global_options->config_file ) &&
        ( ! $self->global_options->config_name ) &&
        ( ! $self->global_options->config_path )
    ) {
        $self->process_env_options;
    }

} # process_global_options


1;
