package Morlock::Command;


# ABSTRACT: A base class for a customized App::Cmd application commands

# COPYRIGHT


use Moose;

extends qw( MooseX::App::Cmd::Command );

with qw( Morlock::Configurable );
with qw( Morlock::Loggable );


=method description

Returns this objects C<abstract> as the default command description.

Override this in your subclass as needed.

=cut

sub description {
    my $self = shift;

    return $self->abstract;
} # description


=method load_config

Returns the configuration information for this command.

=cut

sub load_config {
    my $self = shift;

    my @class = split /::/, ref $self;
    my $config_key = join '::', 'Command', $class[-1];
    my $app_config = $self->app->config;

    return exists($app_config->{$config_key})
            ? $app_config->{$config_key}
            : {};
} # load_config


1;
