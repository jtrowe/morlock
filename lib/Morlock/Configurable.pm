package Morlock::Configurable;


# ABSTRACT: A role which provides configuration information.

# COPYRIGHT


use Moose::Role;


has config => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => 'load_config',
    lazy    => 1,
);


1;
