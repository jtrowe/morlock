package Morlock::ConfigurationLoader;


# ABSTRACT: A role which provides configuration loading logic

# COPYRIGHT


use Moose::Role;

with qw( Morlock::Application );


use Config::General;
use Data::Dump qw( dump );
use File::Basename;
use File::HomeDir;
use File::Spec;
use Try::Tiny;


has config_file => (
    is       => 'rw',
    isa      => 'Str',
    required => 0,
);


has config_name => (
    is      => 'rw',
    isa     => 'Str',
    builder => 'default_config_name',
    lazy    => 1,
);


=method default_config_name

Returns the name of the default configuration file.

=cut

sub default_config_name {
    return 'config';
} # default_config_name


has config_path => (
    is      => 'rw',
    isa     => 'ArrayRef',
    builder => 'default_config_path',
    lazy    => 1,
);


=method default_config_path

Returns the default set of directories which will be searched
for a configuration file.

=cut

sub default_config_path {
    my $self = shift;

    return [
        # FIXME OS portability ?
        File::Spec->catdir('/etc', $self->name),
        File::Spec->catdir(File::HomeDir->my_data, $self->name),
        '.',
    ];
} # default_config_path


=method load_config

Loads the application configuration.

=cut

sub load_config {
    my $self = shift;

    my $c;

    $self->log->debug('load_config');

    my $is_load_by_file = 0;


    my @args;

    if ( my $file = $self->config_file ) {
        $is_load_by_file = 1;
        @args = (
            -ConfigFile => $file,
        ) ;
    }
    else {
        @args = (
            -ConfigFile => $self->config_name,
            -ConfigPath => $self->config_path,
        ) ;
    }

    $self->log->debug(sprintf 'is_load_by_file=%d', $is_load_by_file);

    my %h = (
        @args,
        -IncludeDirectories   => 1,
        -IncludeGlob          => 1,
        -IncludeRelative      => 1,
        -UseApacheInclude     => 1,
        -UTF8                 => 1,
    );
    $self->log->debug("C::G params:\n" . dump(\%h))
            if $self->log->is_debug;

    try {
        $c = Config::General->new(
            @args,
            -IncludeDirectories   => 1,
            -IncludeGlob          => 1,
            -IncludeRelative      => 1,
            -UseApacheInclude     => 1,
            -UTF8                 => 1,
        );
    }
    catch {
        my $e = $_;
        $self->log->error($e);

        $e =~ s/^Config::General\s*//;

        if ( $e =~ m/does not exist within ConfigPath: (.*)! at / ) {
            my $path = $1;
            $path = join ':', @{ $self->config_path };
            $e =~ s/ within ConfigPath: .*/ within ConfigPath: "$path"./;
        }

        if ( $is_load_by_file ) {
            # only throw error if we were configured with a
            # specifically name file
            die $e;
        }
        else {
            # If just searching for a file on the config_path,
            # then it's ok if we don't fine one.

            $e =~ s/^ERROR\s+//;

            $self->log->warn('ALFA : ' . $e);
        }
    };

    unless ( $c ) {
        # Provide an empty config if nothing was loaded to this point
        $c = Config::General->new;
    }

    if ( my @files = $c->files ) {
        my $suffix = $self->config_name;

        if ( $is_load_by_file ) {
            my ( $base, $path ) = fileparse($files[0]);
            $self->config_name($base);
            $self->config_path([ $path ]);
        }
        else {
            # NB: figure out which file is the primary config file
            foreach my $file ( @files ) {
                if ( $file =~ m/$suffix$/ ) {
                    unless ( File::Spec->file_name_is_absolute($file) ) {
                        $file = File::Spec->rel2abs($file);
                    }

                    $self->config_file($file);

                    last;
                }
            }
        } # is_load_by_file
    }

    my %config = $c->getall;

    return \%config;
} # load_config


1;
