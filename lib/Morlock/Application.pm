package Morlock::Application;


# ABSTRACT: A role which provides an application name attribute

# COPYRIGHT


use Moose::Role;

use File::Basename;


has name => (
    is      => 'rw',
    isa     => 'Str',
    builder => 'default_name',
    lazy    => 1,
);


=method default_name

Returns the default application name.

It is derived from the $PROGRAM_NAME variable.

=cut

sub default_name {
    return basename($0);
} # default_name


1;
