package Example::Command::hello;


use Moose;

extends qw( Morlock::Command );


sub execute {
    my ( $self, $opt, $args ) = @_;

    $self->log->info(sprintf 'This is logging from the execute method in %s.',
            __PACKAGE__);

    print STDOUT "Hello!\n";

} # execute


1;
